#include "../cardioda/cardioda.h"
#include <QtTest>


class Cardioda_test : public QObject
{
    Q_OBJECT

public:
    Cardioda_test();
    ~Cardioda_test();

private slots:
    void test_distance_to_center_from_alpha();
    void test_farthest_point();
    void test_radius_of_curvature();

private:
    const float PI = 3.14159265;
};

Cardioda_test::Cardioda_test() {

}

Cardioda_test::~Cardioda_test() {

}

void Cardioda_test::test_distance_to_center_from_alpha() {
    Cardioda a(1);
    QVERIFY(a.distance_to_center_from_alpha(0) < 0.01);
}

void Cardioda_test::test_farthest_point() {
    Cardioda a(1);
    QVERIFY(a.farthest_point() + 4 < 0.01);
}

void Cardioda_test::test_radius_of_curvature() {
    Cardioda a(1);
    float *b = a.radius_of_curvature();
    QVERIFY(abs(b[0] - 1.88) < 0.01);
    QVERIFY(abs(b[1] - 2.66) < 0.01);
    QVERIFY(abs(b[2]) < 0.01);
    delete[] b;
}

QTEST_APPLESS_MAIN(Cardioda_test)

#include "tst_cardioda_test.moc"
