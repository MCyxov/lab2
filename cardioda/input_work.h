#ifndef INPUT_WORK_H
#define INPUT_WORK_H

#include <iostream>


namespace input_work {
    class EOFException {
    public:
        const char error[10] = "EOF error";
    };

    template <typename  T>
    void get_number_in_lain_information(std::string welcome_string, T &out_information) {
        int flag;
        do {
            std::cout << welcome_string;
            std::cin >> out_information;
            if (std::cin.fail()) {
                std::cout << std::endl << "Try again!" << std::endl;
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                flag = 0;
            }
            else if (std::cin.eof()) {
                throw "EOF";
            }
            else
                flag = 1;
        } while (!flag);
        return;
    }

    template <typename  T>
    void get_one_number(T &out_information) {
        int flag;
        do {
            std::cin >> out_information;
            if (std::cin.fail()) {
                std::cout << std::endl << "Try again!" << std::endl;
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                flag = 0;
            }
            else if (std::cin.eof()) {
                throw "EOF";
            }
            else
                flag = 1;
        } while (!flag);
        return;
    }
}

#endif // INPUT_WORK_H
