#ifndef LANGUAGE_H
#define LANGUAGE_H

namespace welcome_strings {
    const char MENU[] = "0 - exit\n"
                        "1 - get distance to center from alpha\n"
                        "2 - get coordinates of points farthest from the cardioid axis\n"
                        "3 - get the radius of curvature at the characteristic points of the cardioid\n"
                        "4 - get the area described by the cardioid\n"
                        "5 - get the length of the cardioid arc based on the angle of the polar radius\n"
                        "Enter command: ";
    const char GET_COEFFICIENT[] = "Input coefficient: ";
    const char BYE[] = "BYE!";
    const char ANSWER[] = "Answer: ";
    const char BAD_NUMBER[] = "Bad number";
    const char BIG_ANSWER[] = "Radius of curvature:";
    const char GET_ANGLE[] = "Input angle: ";
}

#endif // LANGUAGE_H
