#include "input_work.h"
#include "language.h"
#include "cardioda.h"


int main() {
    float a, buf1;
    int buf;
    input_work::get_number_in_lain_information(welcome_strings::GET_COEFFICIENT, a);
    if (a == 0) {
        std::cout << welcome_strings::BAD_NUMBER << std::endl;
        return 0;
    }
    Cardioda card(a);
    do {
        try {
            input_work::get_number_in_lain_information(welcome_strings::MENU, buf);
            switch (buf) {
            case 0:
                std::cout << welcome_strings::BYE << std::endl;
                return 0;
            case 1:
                input_work::get_number_in_lain_information(welcome_strings::GET_COEFFICIENT, buf1);
                std::cout << welcome_strings::ANSWER << card.distance_to_center_from_alpha(buf1) << std::endl;
                break;
            case 2:
                std::cout << welcome_strings::ANSWER << "(" <<  card.farthest_point() << ", 0.0)" << std::endl;
                break;
            case 3: {
                float *buf2 = card.radius_of_curvature();
                std::cout << welcome_strings::BIG_ANSWER << std::endl << buf2[0] << std::endl << buf2[1] << std::endl << buf2[3] << std::endl;
                delete[] buf2;
                break;
            }
            case 4:
                std::cout << welcome_strings::ANSWER << card.area_described() << std::endl;
                break;
            case 5:
                input_work::get_number_in_lain_information(welcome_strings::GET_ANGLE, buf1);
                std::cout << welcome_strings::GET_ANGLE << card.length_of_the_cardioid(buf1) << std::endl;
                break;
            default:
                std::cout << "Undefined command, try again!" << std::endl;
                break;
            }
        }  catch (input_work::EOFException e) {
            std::cout << e.error << std::endl;
            return 0;
        }

    } while (true);
    return 0;
}
