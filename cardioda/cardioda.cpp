#include "cardioda.h"

Cardioda::Cardioda(float a) {
    this->a = a;
}

float Cardioda::distance_to_center_from_alpha(float phi) {
    return 2 * this->a * (1 - cos(phi));
}

float Cardioda::farthest_point() {
    return -4*this->a;
}

float *Cardioda::radius_of_curvature(){
    float *radius = new float[3];
    radius[0] = 4*sqrt(2)*this->a/3;
    radius[1] = 8*this->a/3;
    radius[2] = 0.0;
    return radius;
}

float Cardioda::area_described() {
    return 6*this->PI*this->a*this->a;
}

float Cardioda::length_of_the_cardioid(float phi) {
    return 16*this->a*(1-cos(phi/2));
}
