QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app


SOURCES += \
        ../cardioda/cardioda.cpp \
        tst_cardioda_test.cpp

HEADERS += \
        ../cardioda/cardioda.h

