TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle

SOURCES += \
        cardioda.cpp \
        main.cpp

HEADERS += \
        cardioda.h \
        input_work.h \
        language.h
