#ifndef CARDIODA_H
#define CARDIODA_H

#include <cmath>
#include <QObject>


class Cardioda  : public QObject {
    Q_OBJECT
public:
    Cardioda(float a);
    float distance_to_center_from_alpha(float phi);
    float farthest_point();
    float* radius_of_curvature();
    float area_described();
    float length_of_the_cardioid(float phi);

private:
    const float PI = 3.14159265;
    float a;
};

#endif // CARDIODA_H
